<?php

namespace App\Form;

use Doctrine\DBAL\Types\FloatType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

class BoatSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('searchString', TextType::class, [
                'constraints'=> new NotNull(['message'=> 'Veuillez rechercher quelque chose !'])
            ])
            ->add('longeurMax', NumberType::class, [
                'required'=> false
            ])
            ->add('longeurMin', NumberType::class, [
                'required'=> false
            ])
            ->add('nbChevauxMax', IntegerType::class, [
                'required'=> false
            ])
            ->add('nbChevauxMin', IntegerType::class, [
                'required'=> false
            ])
            ->add('isVoilier', ChoiceType::class, [
                'choices'=> [
                    'Oui'=> true,
                    'Non'=> false,
                    'Peu importe'=> null
                ],
                'expanded'=> true
            ])
            ->add('submit', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
