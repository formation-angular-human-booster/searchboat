<?php

namespace App\Controller;

use App\Form\BoatSearchType;
use App\Repository\BoatRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    /**
     * @Route("/search", name="search")
     */
    public function index(Request $request, BoatRepository $boatRepository)
    {
        $formSearch = $this->createForm(BoatSearchType::class);

        $formSearch->handleRequest($request);

        if($formSearch->isSubmitted() and $formSearch->isValid()){
            $data = $formSearch->getData();
            $datas = $boatRepository->findComplexSearch($data);

            return $this->render('search/index.html.twig', [
                'controller_name' => 'SearchController',
                'formSearch'=> $formSearch->createView(),
                'datas'=> $datas
            ]);

        }

        return $this->render('search/index.html.twig', [
            'controller_name' => 'SearchController',
            'formSearch'=> $formSearch->createView()
        ]);
    }
}
