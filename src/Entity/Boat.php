<?php

namespace App\Entity;

use App\Repository\BoatRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BoatRepository::class)
 */
class Boat
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $modele;

    /**
     * @ORM\Column(type="float")
     */
    private $longueur;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbChevaux;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isVoilier;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getModele(): ?string
    {
        return $this->modele;
    }

    public function setModele(string $modele): self
    {
        $this->modele = $modele;

        return $this;
    }

    public function getLongueur(): ?float
    {
        return $this->longueur;
    }

    public function setLongueur(float $longueur): self
    {
        $this->longueur = $longueur;

        return $this;
    }

    public function getNbChevaux(): ?int
    {
        return $this->nbChevaux;
    }

    public function setNbChevaux(?int $nbChevaux): self
    {
        $this->nbChevaux = $nbChevaux;

        return $this;
    }

    public function getIsVoilier(): ?bool
    {
        return $this->isVoilier;
    }

    public function setIsVoilier(bool $isVoilier): self
    {
        $this->isVoilier = $isVoilier;

        return $this;
    }
}
