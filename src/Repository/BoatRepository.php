<?php

namespace App\Repository;

use App\Entity\Boat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Boat|null find($id, $lockMode = null, $lockVersion = null)
 * @method Boat|null findOneBy(array $criteria, array $orderBy = null)
 * @method Boat[]    findAll()
 * @method Boat[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BoatRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Boat::class);
    }

    public function findComplexSearch($filters) {
        $query =  $this->createQueryBuilder('boat')
            ->where('boat.nom LIKE :searchString')
            ->orWhere('boat.modele LIKE :searchString')
            ->setParameter('searchString', '%'.$filters['searchString'].'%');

        if(!is_null($filters['longeurMax'])){
            $query->andWhere('boat.longueur < :valueMax')
                ->setParameter('valueMax', $filters['longeurMax']);
        }

        if(!is_null($filters['longeurMin'])){
            $query->andWhere('boat.longueur > :longueurMin')
                ->setParameter('longueurMin', $filters['longeurMin']);
        }

        if(!is_null($filters['nbChevauxMax'])){
            $query->andWhere('boat.nbChevaux < :nbChevauxMax')
                ->setParameter('nbChevauxMax', $filters['nbChevauxMax']);
        }

        if(!is_null($filters['nbChevauxMin'])){
            $query->andWhere('boat.nbChevaux > :nbChevauxMin')
                ->setParameter('nbChevauxMin', $filters['nbChevauxMin']);
        }

        if(!is_null($filters['isVoilier'])) {
            $query->andWhere('boat.isVoilier = :isVoilier')
                ->setParameter('isVoilier', $filters['isVoilier']);
        }


        return $query ->getQuery()->getResult();

    }

    // /**
    //  * @return Boat[] Returns an array of Boat objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Boat
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
